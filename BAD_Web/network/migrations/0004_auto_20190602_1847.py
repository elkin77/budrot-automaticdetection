# Generated by Django 2.2 on 2019-06-02 23:47

from django.db import migrations, models
import network.models


class Migration(migrations.Migration):

    dependencies = [
        ('network', '0003_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='output_file',
            field=models.FileField(max_length=500, upload_to=network.models.create_path),
        ),
    ]
